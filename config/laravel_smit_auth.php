<?php

return [
    'domain' => env('SMIT_AUTH_DOMAIN', 'auth.smit.net'),
    'client_id' => env('SMIT_AUTH_CLIENT_ID'),
    'client_secret' => env('SMIT_AUTH_CLIENT_SECRET'),
    'redirect_uri' => env('SMIT_AUTH_REDIRECT_URI'),
];
