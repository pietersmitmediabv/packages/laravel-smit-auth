<?php

namespace Smit\LaravelSmitAuth\Test;

use Dotenv\Dotenv;
use Orchestra\Testbench\TestCase as Orchestra;
use Smit\LaravelSmitAuth\LaravelSmitAuthServiceProvider;
use Smit\LaravelSmitAuth\Middleware\SmitAuthenticate;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->setupDummyRoutes();
        $this->setupEnv();
        $_SERVER['HTTP_HOST'] = 'localhost';
        $_SERVER['REQUEST_URI'] = '';
    }

    protected function getPackageProviders($app)
    {
        return [LaravelSmitAuthServiceProvider::class];
    }

    protected function setupDummyRoutes()
    {
        $this->app['router']->group(
            ['middleware' => SmitAuthenticate::class],
            function () {
                $this->app['router']->get('/smitauth/dummy', function () {
                    return 'Hello world!';
                });
            }
        );
    }

    protected function setupEnv()
    {
        $env = Dotenv::create(__DIR__ . '/../')->load();

        config(['laravel_smit_auth.domain' => $env['SMIT_AUTH_DOMAIN']]);
        config(['laravel_smit_auth.client_id' => $env['SMIT_AUTH_CLIENT_ID']]);
        config(['laravel_smit_auth.client_secret' => $env['SMIT_AUTH_CLIENT_SECRET']]);
        config(['laravel_smit_auth.redirect_uri' => $env['SMIT_AUTH_REDIRECT_URI']]);
    }
}
