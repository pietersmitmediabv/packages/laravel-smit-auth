<?php

namespace Smit\LaravelSmitAuth\Test\Feature;

use Orchestra\Testbench\TestCase;
use Smit\LaravelSmitAuth\LaravelStore;

class LaravelStoreTest extends TestCase
{
    /** @test */
    public function it_can_store_and_retrieve_a_value_by_a_key()
    {
        $store = new LaravelStore();
        $value = 'Goeiemorgen!';
        $key = 'testing';

        $store->set($key, $value);

        $this->assertEquals($store->get($key), $value);
    }
}
