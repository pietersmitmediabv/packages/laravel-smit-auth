<?php

namespace Smit\LaravelSmitAuth\Test\Feature;

use Illuminate\Http\Request;
use Smit\LaravelSmitAuth\Auth\SmitAuth;
use Smit\LaravelSmitAuth\Middleware\SmitAuthenticate;
use Smit\LaravelSmitAuth\Test\TestCase;

class AuthConnectionTest extends TestCase
{
    /** @test */
    public function it_redirects_to_login_when_unauthenticated()
    {
        $client = new SmitAuth();

        $location = $this->get('/smitauth/dummy')->headers->get('Location');
        $baseRoute = explode('?', $location)[0];

        $this->assertEquals($baseRoute, $client->route('authorize'));
    }

    /** @test */
    public function it_()
    {
        $client = new SmitAuth();

        // $this->followingRedirects()
        // ->get('/smitauth/dummy')

    }
}
