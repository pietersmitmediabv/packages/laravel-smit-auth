<?php


use Smit\LaravelSmitAuth\Controllers\LaravelSmitAuthController;

Route::get('/smitauth/callback', [LaravelSmitAuthController::class, 'callback']);
Route::get('/smitauth/logout', [LaravelSmitAuthController::class, 'logout']);
