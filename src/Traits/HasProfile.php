<?php

use Smit\LaravelSmitAuth\Models\Profile;

trait HasProfile
{
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

}
