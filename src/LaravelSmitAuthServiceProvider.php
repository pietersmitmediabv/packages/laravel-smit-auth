<?php

namespace Smit\LaravelSmitAuth;

use Illuminate\Support\ServiceProvider;

class LaravelSmitAuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton('SmitAuth', function ($app) {
            return new SmitAuth();
        });

        $this->publishes([
            __DIR__ . '/../config/laravel_smit_auth.php' => config_path('laravel_smit_auth'),
        ], 'laravel_smit_auth_config');

        // $this->loadMigrationsFrom(__DIR__ . '/Migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/laravel_smit_auth.php',
            'laravel_smit_auth'
        );
    }
}
