<?php

namespace Smit\LaravelSmitAuth\Middleware;

use Closure;
use Illuminate\Http\Request;
use Smit\LaravelSmitAuth\Facade\SmitAuth;

class SmitAuthenticate
{
    public function handle(Request $request, Closure $next)
    {
        if (! SmitAuth::isLoggedIn()) {
            return SmitAuth::login();
        }

        return $next($request);
    }
}

