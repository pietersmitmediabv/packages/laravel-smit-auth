<?php

namespace Smit\LaravelSmitAuth\Controllers;

use Illuminate\Routing\Controller;
use Smit\LaravelSmitAuth\SmitAuth;

class LaravelSmitAuthController extends Controller
{
    public function callback(SmitAuth $client)
    {
        return $client->callback();
    }

    public function logout(SmitAuth $client)
    {
        return $client->logout('/beheer');
    }
}
