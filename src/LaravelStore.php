<?php

namespace Smit\LaravelSmitAuth;

use Illuminate\Support\Str;
use SMIT\SDK\Auth\Stores\StoreInterface;

class LaravelStore implements StoreInterface
{
    public const KEY_NAMESPACE = 'laravel_smit_auth.';

    public function set($key, $value)
    {
        session()->put($this->namespaceKey($key), $value);
    }

    public function get($key, $default = null)
    {
        return session()->get($this->namespaceKey($key), $default);
    }

    public function delete($key)
    {
        session()->remove($this->namespaceKey($key));
    }

    public function flush()
    {
        collect(session()->all())->each(function ($key) {
            if (Str::startsWith($key, self::KEY_NAMESPACE)) {
                session()->remove($key);
            }
        });
    }

    protected function namespaceKey($key)
    {
        return self::KEY_NAMESPACE . $key;
    }
}
