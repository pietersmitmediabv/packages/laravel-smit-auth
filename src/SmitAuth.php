<?php

namespace Smit\LaravelSmitAuth;

use Smit\LaravelSmitAuth\Exceptions\AuthConfigurationException;
use SMIT\SDK\Auth\Auth;
use SMIT\SDK\Auth\Models\UserModel;

class SmitAuth extends Auth
{
    public function __construct()
    {
        $this->guardAgainstIncompleteConfig();

        parent::__construct([
            'domain' => config('laravel_smit_auth.domain'),
            'client_id' => config('laravel_smit_auth.client_id'),
            'client_secret' => config('laravel_smit_auth.client_secret'),
            'redirect_uri' => config('laravel_smit_auth.redirect_uri'),
        ]);
    }

    public function user()
    {
        $user = parent::user();

        if (! $user instanceof UserModel) {
            return null;
        }

        return $user;
    }

    public function redirect(string $url = null, array $queryParameters = [])
    {
        if (!empty($queryParameters)) {
            $this->setQueryParameters($queryParameters);
        }

        if (!is_null($url)) {
            $this->setRedirectUrl($url);
        }

        return redirect($this->getRedirectUrl());
    }

    protected function guardAgainstIncompleteConfig()
    {
        $keys = collect(['client_id', 'client_secret', 'redirect_uri']);

        $keys->each(function ($key) {
            if (config('laravel_smit_auth.' . $key) === null) {
                throw new AuthConfigurationException($key);
            }
        });
    }
}
