<?php

namespace Smit\LaravelSmitAuth\Facade;

use Illuminate\Support\Facades\Facade;
use SMIT\SDK\Auth\Models\UserModel;
use SMIT\SDK\Auth\Stores\StoreInterface;

/**
 * @method static StoreInterface store()
 * @method static void callback()
 * @method static UserModel|null user()
 * @method static void login(array $scopes, string $returnto = null)
 * @method static void logout(string $returnTo = null, $federated = false, array $options = [])
 *
 * @see \Smit\LaravelSmitAuth\SmitAuth
 */
class SmitAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SmitAuth';
    }
}
