<?php

namespace Smit\LaravelSmitAuth\Exceptions;

use Exception;

class AuthConfigurationException extends Exception
{
    public function __construct($key)
    {
        parent::__construct("Missing config key '{$key}'");
    }
}
